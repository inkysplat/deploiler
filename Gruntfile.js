
module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            site: {
                src: [
                    'themes/default/assets/js/script.js'],
                dest: 'themes/default/assets/js/<%= pkg.name %>.concat.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                preserveComments: false,
                mangle: false
            },
            site: {
                src: '<%= concat.site.dest %>',
                dest: 'themes/default/assets/js/<%= pkg.name %>.min.js'
            }
        },
        clean: [
            '<%= concat.site.dest %>',
            '<%= uglify.site.dest %>',
            '<%= cssmin.minify.src %>',
            '<%= cssmin.minify.dest %>'
        ],
        copy: {
            main: {
                expand: true,
                flatten: true,
                filter: 'isFile',
                src: 'themes/default/assets/vendor/font-awesome/fonts/fontawesome-webfont*',
                dest: 'themes/default/assets/fonts/'
            }
        },
        fontAwesomeVars: {
            main: {
                variablesLessPath: 'themes/default/assets/vendor/font-awesome/less/variables.less',
                fontPath: '../fonts'        //NOTE: this must be relative to FINAL, compiled .css file - NOT the variables.less file! For example, this would be the correct path if the compiled css file is main.css which is in 'src/build' and the font awesome font is in 'src/bower_components/font-awesome/fonts' - since to get from main.css to the fonts directory, you first go back a directory then go into bower_components > font-awesome > fonts.
            }
        },
        cssmin: {
            combine: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                    keepSpecialComments: 0
                },
                files: {
                    'themes/default/assets/css/<%= pkg.name %>.concat.css': [
                        'themes/default/assets/css/styles.css'
                    ]
                }
            },
            minify: {
                src: 'themes/deafult/assets/css/<%= pkg.name %>.concat.css',
                dest: 'themes/default/assets/css/<%= pkg.name %>.min.css'
            }
        },
        watch: {
            scripts: {
                files: ['**themes/default/assets/js/*.js'],
                tasks: ['concat','uglify'],
                options: {
                    spawn: false
                }
            },
            styles: {
                files: ['**themes/default/assets/css/*.css'],
                tasks: ['cssmin'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-font-awesome-vars');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Default task(s).
    grunt.registerTask('default', ['clean', 'copy','fontAwesomeVars','cssmin','concat','uglify']);
    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });

};
